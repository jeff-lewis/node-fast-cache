#ifndef V8_HASH_H
#define V8_HASH_H

#if _WIN64 || __WIN64 || __WIN64__ || __amd64__ || __x86_64__ || __ppc64__
#define ENV_64
#define SEED 0x1a2b3c4d5e6f0a3f
#else
#define SEED 0x1a2b3c4d
#endif

#include <node.h>
using namespace v8;
#include <string>

#ifdef __APPLE__
#include <tr1/unordered_map>
#include <list>
#define unordered_map std::tr1::unordered_map
#define list std::list
#else
#include <unordered_map>
#include <list>
#define unordered_map std::unordered_map
#define list std::list
#endif

#include "xxhash.h"

struct v8_key_hash
{
    size_t operator()(Handle<Value> key) const
    {
        String::Utf8Value utf(key);

#ifdef ENV_64
        return XXH64(*utf, utf.length(), SEED);
#else
        return XXH32(*utf, utf.length(), SEED);
#endif
    }
};

struct v8_key_equality
{
    bool operator()(Handle<Value> a, Handle<Value> b) const
    {
        // This should be sufficient enough
        return a->Equals(b);
    }
};

#endif // V8_HASH_H
